# Function to check and install Node.js
function Check-Node {
    if (-not (Get-Command node -ErrorAction SilentlyContinue)) {
        $installNode = Read-Host "Node.js is not installed. Would you like to install Node.js now? (yes/no)"
        if ($installNode -eq "yes") {
            Write-Host "Installing Node.js..."
            Start-Process powershell -ArgumentList "choco install nodejs-lts -y" -Verb RunAs -Wait
        } else {
            Write-Host "Node.js is required to run KADI. Exiting..."
            exit 1
        }
    } else {
        Write-Host "Node.js is already installed."
    }
}

# Function to check and install Git
function Check-Git {
    if (-not (Get-Command git -ErrorAction SilentlyContinue)) {
        $installGit = Read-Host "Git is not installed. Would you like to install Git now? (yes/no)"
        if ($installGit -eq "yes") {
            Write-Host "Installing Git..."
            Start-Process powershell -ArgumentList "choco install git -y" -Verb RunAs -Wait
        } else {
            Write-Host "Git is required to clone repositories. Exiting..."
            exit 1
        }
    } else {
        Write-Host "Git is already installed."
    }
}

# Check and install Node.js and Git if necessary
Check-Node
Check-Git

# Store the current working directory
$INSTALL_DIR = Get-Location
Write-Host "Installation directory: $INSTALL_DIR"

# Ask user if they want to install and use a local kadi-server
$useLocalServer = Read-Host "Do you want to install and use a local kadi-server? (yes/no)"

if ($useLocalServer -eq "yes") {
    # Clone kadi-server repository
    git clone https://gitlab.com/humin-game-lab/kadi/kadi-server.git "$INSTALL_DIR\kadi-server"
    Set-Location "$INSTALL_DIR\kadi-server"
    npm install

    # Start server in background and save PID
    Start-Process -FilePath "node" -ArgumentList "server.js" -NoNewWindow -PassThru | ForEach-Object {
        $SERVER_PID = $_.Id
    }
    Write-Host "kadi-server started with PID $SERVER_PID"

    # Pause to give server time to start
    Start-Sleep -Seconds 5
} else {
    # Ask for the public server URL and port
    $SERVER_URL = Read-Host "Please provide the URL and port for the public server (in the form http://myserver.com:port)"
}

# Change back to install directory
Set-Location "$INSTALL_DIR"

# Clone kadi repository
git clone https://gitlab.com/humin-game-lab/kadi/kadi.git "$INSTALL_DIR\kadi"
Set-Location "$INSTALL_DIR\kadi"

# If using a remote server, update the agent.json file
if ($useLocalServer -eq "no") {
    if (Test-Path "agent.json") {
        Write-Host "Updating agent.json to use the remote server URL."
        (Get-Content agent.json) -replace '"api": ".*"', '"api": "' + $SERVER_URL + '"' | Set-Content agent.json
    } else {
        Write-Host "Warning: agent.json file not found. Skipping update."
    }
}

# Install dependencies and setup KADI CLI
npm install
Set-Location "abilities\kadi-install\0.0.0"
npm install

# Link the kadi CLI
Set-Location "$INSTALL_DIR\kadi"
npm link

# Run initial KADI setup commands
kadi install
kadi update

# If using a local server, restart the server in foreground
if ($useLocalServer -eq "yes") {
    Stop-Process -Id $SERVER_PID -Force
    Write-Host "Restarting kadi-server in foreground..."
    node server.js
}

Write-Host "KADI CLI and server setup is complete."
