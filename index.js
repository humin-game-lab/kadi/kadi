#!/usr/bin/env node

const path = require('path');
const fs = require('fs')
const fsPromises = require('fs').promises;

async function kadi() {
    //Get versions of modules
    const { getKadiJSON } = await import('kadi-core');

    const agentJSON = getKadiJSON();
    const bin = agentJSON.bin;
    
    //Break up the command line arguments
    const [,, command, ...args] = process.argv;
    console.log('bin[command]:', path.join(__dirname, bin[command]));
    if(!fs.existsSync(path.join(__dirname, bin[command]))) {
        console.error(`No installed version for ${command}.`);
        process.exit(1);
    }

    try {
        // const modulePath = path.join(__dirname, 'abilities', moduleName, config[moduleName], 'index.js');
        const modulePath = path.join(__dirname, bin[command], 'index.js');
        //Debug output to note which module is being processed
        console.log(`Calling: ${command}\nAt: ${modulePath} \n`);
        
        //Load the module and pass the arguments
        const module = await import(modulePath);
        await module.default(args);

    } catch (error) {
        console.error(`Error: ${error.message}`);
        process.exit(1);
    }
}

kadi();

