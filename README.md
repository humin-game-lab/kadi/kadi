# K&#x100;DI: Knowledge and Ability Deployment Interface


## Description
-----------

K&#x100;DI is a versatile command-line tool designed for managing agent abilities, simplifying dependency management, and automating agent setup and execution processes. It facilitates the initialization of agent projects, installation of abilities with dependency resolution, and execution of custom commands defined within agent configurations.

## Features
--------

*   **Initialization**: Set up a new agent project with customizable settings.
*   **Installation**: Manage abilities and their dependencies efficiently.
*   **Execution**: Run predefined custom commands for initialized agents.



[**install**](https://gitlab.com/humin-game-lab/kadi/kadi-install)
During install all abilities will be downloaded, unpacked and placed in the `abilities` folder (which will be created if needed).  Each ability downloaded will have all dependencies installed recursively.  Once an ability is installed, it will not be downloaded or reinstalled again, even if later dependencies/abilities require it.

The test server provides some base abilities that can be downloaded/modified for testing.  All abilities will be stored in a zip file that kadi can download and extract.  These abilities can then be linked directly into the agent that is being created.

[**init**](https://gitlab.com/humin-game-lab/kadi/kadi-init)
the init command line string will be executed upon install and all abiliites/dependencies have been downloaded, unpacked and initialized (by calling their own init command line string).

Each ability installed will have its `init` command line called once all dependencies have been met.  The calls are made recursively and will end with the `init` command line string from `agent.json` being called.

[**run**](https://gitlab.com/humin-game-lab/kadi/kadi-run)
These are specific commands you can register for the ability that can be be called directly from the cli.

[**update**](https://gitlab.com/humin-game-lab/kadi/kadi-update)
Will update the agent or specific named ability to latest or specified vesion 

**start**
This is the command line string to be called for starting agent

### agent.json
The agent.json file holds all configuraiton data for an agent and/or ability/tool.  Below is a sample `agent.json` provided in the source folder as well as in the test server.  This provides all the information needed to initialize and run the agent.

```json
{
  "name": "SampleAgent",
  "version": "1.0.0",
  "license": "MIT",
  "description": "A sample agent to demonstrate the kadi install feature.",
  "repo": "https://gitlab.com/humin-game-lab/kadi/kadi-install.git",
  "lib": "https://gitlab.com/humin-game-lab/kadi/kadi-install/-/archive/0.0.0/kadi-install-0.0.0.zip",
  "api": "http://localhost:3000",
  "abilities": [
    {
      "name": "kadi-install",
      "version": "0.0.1"
    }
  ],
  "bin": {
    "kadi": "index.js",
    "install": "abilities/kadi-install/0.0.1"
  },
  "setup": "echo 'SampleAgent 1.0.0 is starting...'",
  "init":  "echo 'SampleAgent 1.0.0 has completed...'",
  "start": "echo 'Starting SampleAgent...'",
  "run": {
    "greet": "echo 'Hello, World from SampleAgent!'",
    "checkDependencies": "echo 'Checking dependencies...'"
  }
}
```

Commands
--------

### `kadi init`

Initializes a new agent project by generating an `agent.json` file based on user input. It prompts for basic agent information, `init` command strings for setup operations, and `run` commands for operational actions.

**Example:**

`kadi init`

This command will ask for:

*   Agent Name, Version Number, License Type, and Description.
*   An `init` command line string for post-installation setup.
*   Multiple `run` command line strings for different operational actions, which users can add as needed.


### [`kadi install [abilityName] [version]`](https://gitlab.com/humin-game-lab/kadi/kadi-install)

Installs an ability. If an ability name is provided, it installs that specific ability. Without specifying an ability name, it installs all abilities listed in the `agent.json`'s abilites.

**Example:**

*   Install a specific ability:
    
    
    `kadi install logging`
    
*   Install all abilities from `agent.json`:
    
    
    `kadi install`
    

After installing abilities and their dependencies, the `init` command specified in the `agent.json` of each ability (if any) is executed to perform necessary setup tasks.


### [`kadi update [abilityName]`](https://gitlab.com/humin-game-lab/kadi/kadi-update)

Updates an ability. If an ability name is provided, it update that specific ability. Without specifying an ability name, it updates all abilities listed in the `agent.json`'s abilites.

**Example:**

*   Update a specific ability:
    
    `kadi update logging`
    
*   Update all abilities from `agent.json`:
    
    `kadi update`

    
### [`kadi run [key] [-a abilityName]`](https://gitlab.com/humin-game-lab/kadi/kadi-run)

Executes a custom command specified under the `run` object in the `agent.json` file. The `[key]` parameter corresponds to the action name defined in `run`.  If no `key` is defined, then it will attempt to execute the `start` command within the `agent.json` file. `[key]` can also be `init` or `setup` to run default scripts asscoiated with those fields in agent.json.

Scripts from abilities can also be run by using the `-a` (or `--a`, `-ability`, `--ability`) flag, followed by the name of the ability.  This will run the script associated with the `key` (or the `start` script if no `key` is provided) from the named ability


**Examples:**

*    To execute the `greet` action defined in the `run` property within the `agent.json` :

`kadi run greet`

*    To run the default `start` command listed in the `agent.json`

`kadi run`
or
`kadi run start`

*   To run the default `start` command listed in an ability, either of teh following would work

`kadi run -a myAbility`
or
`kadi run start -a myAbility`

Development and Testing with K&#x100;DI
================================

[**kadi-server**](https://gitlab.com/humin-game-lab/kadi/kadi-server)
The `kadi-server` prvoides a local server that can be used to store agent.json files, and allows your kadi installation to operate outside of the live production server.  You can also deploy the server to create your own private instance of kadi, hosting your own abilities privately outside of the publice kadi system.


Installation Instructions
-------------------------
kadi is dependent on node.js and git being installed on your system.

### Instructions for Using the Installation Scripts

Here are the instructions for downloading and running the installation scripts 

#### For Linux/macOS Users

1. Open a terminal window.
2. Use the `curl` command to download the installation script:
   ```
   curl -o install_kadi.sh http://agents.hewmen.ai/uploads/install_kadi_linux.sh
   ```
3. Make the script executable:
   ```
   chmod +x install_kadi.sh
   ```
4. Run the script:
   ```
   ./install_kadi.sh
   ```
5. Follow any on-screen instructions to complete the installation.

#### For Windows Users

1. Open Command Prompt.
2. Use the following command to download the installation script. You might need a third-party tool like `wget` or use a browser to download the script manually due to the lack of a native command-line download tool in some versions of Windows:
   ```
   [Your download method] http://agents.hewmen.ai/uploads/install_kadi_win.ps1
   ```
3. Navigate to the directory where you downloaded `install_kadi_win.ps1`.
4. Run the script:
   ```
   .\install_kadi_win.ps1
   ```
5. Follow any on-screen instructions to complete the installation.

### Note

- These scripts will download both `kadi` and `kadi-server` repos and perform installations on them - The repos will be downloaded into the same directory from where the instalation script is being called.
- You must have Node.js installed before running the installation script. If on linxu/macOS, the script will install nvm (node version manager), and install the latest version of node and npm, then complete teh installation process.  Windows will provide an error message and instruct to preform node installation before continuing
- The script will startup a local instance of the `kadi-server` running on port 3000, if this fails, kadi will fail to install properly, as it is currently set by default to look for updates from localhost.  You can rerun `kadi install` from the root `kadi` directory once the server is running, and the installation process will complete


Uninstall
---------

### Linux and macOS

`npm unlink kadi -g rm -rf ./kadi ./kadi-server`

### Windows

Open PowerShell as an Administrator and run:

`npm unlink kadi -g`
`Remove-Item -Path .\kadi -Recurse -Force`
`Remove-Item -Path .\kadi-server -Recurse -Force`


Roadmap Features
---------

* Front End
    * Search for abilities/agents that are loaded
    * Create Abilities: adding/editing `abilities.json` from front end
    * release builds should be hosted from GitHub/Lab repo, using Release functionality.
* Features
    * Fully support versioning 
        * storage 
        * ~~download~~ 
        * searching 
        * ~~installing~~
    * Create DB for storing `ability.json` data, and enable searching
    * Update endpoints:
        * ~~/get used to download ability, shold support version number~~
        * /search provides top matches based upon search criterion
        * ~~serve install scripts~~
    

* **Refactor**

    * Create default abilities for K&#x100;DI
        * ~~install~~
        * ~~update~~
        * ~~run~~
        * init
    * kadi-core
        * what core functionality should be part of kadi-core, and what should be an external module/abilitiy
            * broker, ipc, agent.json getters, process spawner, etc
            * should kadi-core just be a wrapper over kadi ability extensions?
            * only feature that absolutely needs to be an npm module is the loadAbility function
        * should broker and ipc be part of npm module, or abiliites
        * if ability, does kadi-core install them
            * init: npm install, start: kadi install?
            * each ability would then probalby need to load kadi-core as a module (to use agent.json getter, process spawner, etc).
        * should they be stand alone npm modules?
        * Once kadi-core and dependencies are done, kadi (and all commands), need to be rewritten, using the kadi-core module.  this will be v1.0 release
    * Update agent.json to allow for more details of the abilities

        ```json
        {
        "ability": [
            {
            "name": "ExampleModule",
            "version": "1.0.0",
            "language": "python",
            "runtimeEnvironment": "Python",
            "container": "docker",
            "operatingSystem": {
                "name": "linux",
                "version": ">=18.04"
            },
            "dependencies": {
                "environment": "miniconda",
                "packages": ["numpy", "pandas"]
            },
            "additionalInfo": {
                "requiresGUI": false,
                "networkAccess": true
            }
            }
        ]
        }
        ```
    * Update install scripts to ask user if they want to install a local dev server, if not it should now point to the live kadi server being hosted.
        
    * Add support for broker
        * ~~Will need have ability to query broker to see if remote agents/abilities are online and available, as well as register once local agents have been initizlized~~
        * Agent/Abilities should be installed if wanting to run locally, or just the broker interface for communication should be installed if running remotely
            * need to update `agent.json` to denote the type of install, as well update the `install` functions to switch based upon type
            * Also requires two zip folders, one for local, one for remote access
            * On install, each agent will need to create a key (maybe public/private pair), this will be used to control access to agent/ability based upon it privacy/sharing settings
        * Update ability/agent install to provide clarity on how it will be utilized
            * local: will be installed and running direclty on machine
            * remote: will utilize an existing agent/ability on network, and will only need communication interface, not full abilty/agent install code
            * private: This agent/ability will only be accissble to agent being installed, and no other agent/ability can communicate to it, broker will block on all communication to agent/ability outside of current agent key
            * public: This agent/ability will be shared on network and any other agent can communicate and send task to agent/ability
            * restricted: This agent/ability is on network, but only authorized agents/abilities can communicate wtih it (will need to utilize keys)
        * Broker system will need to be able to dynamcially build messages using something like this:

```javascript
        CreateAndSendMessage( LLMOuput request) { 
        //request is the output of an LLM requesting some task be done and gives a name of the ability that should be used to accomplish the request (i.e. where the message needs to be sent)

        //By looking at the LLM request, determine what the name of the ability is that needs
        //   a message sent too
        GetAbility(request){
            name = GetAbilityName(LLM("return name of ability from the following text: " + request)));
    
            return ({ability: name, request: requst});
        }->then(input){
        // Get abilities schema and single shot example
    
        //Using the name of Ability, query to get its schema and single shot example
        //. Broker has access to all of this, because they are loaded into the agent, 
        //    and listed in the agent.json
        schema = GetSchema(input.ability);
        example = GetExample(input.ability);
        request += "Create a message for the {{request}} below," +
        " using the {{schema}} provided, " +           
        " follwing this {{example}}:" +
        "Request: " + request + "/n";
        "Schema: " + schema + "/n";
        "Example: " + example + "/n";
    
        message = GetMessage(LLM(request))
    
        return message;
    
    } --> then(input){
      //Send the message

        //Take the formatted message and send it out
        broker.sendMessage(input);
    }
}
```

Tools Schema
```json
tools = [
    {
        "type": "function",
        "function": {
            "name": "search",
            "description": "perform a  search using the web",
            "parameters": {
                "type": "object",
                "properties": {
                    "query": {
                        "type": "string",
                        "description": "The search query",
                    },
                },
                "required": ["query"],
            }
        }
    },
    {
        "type": "function",
        "function": {
            "name": "get_current_weather",
            "description": "Get the current weather in a given location",
            "parameters": {
                "type": "object",
                "properties": {
                    "location": {
                        "type": "string",
                        "description": "The city and state, e.g. San Francisco, CA",
                    },
                    "unit": {
                        "type": "string", 
                        "enum": ["celsius", "fahrenheit"]},
                },
                "required": ["location"],
            },
        },   
    }
]

```